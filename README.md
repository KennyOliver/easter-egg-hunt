# easter-egg-hunt :rabbit: :hatching_chick:

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/easter-egg-hunt/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/easter-egg-hunt?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/easter-egg-hunt?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/easter-egg-hunt?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/easter-egg-hunt)](https://replit.com/@KennyOliver/easter-egg-hunt)

**Easter Eggs have been hidden, and your Easter Bunny will have to find them!**

## VividHues :rainbow: :package:
**easter-egg-hunt** uses **VividHues** - my own Python Package!

[![VividHues](https://img.shields.io/badge/Get%20VividHues-252525?style=for-the-badge&logo=python&logoColor=white&link=https://github.com/KennyOliver/VividHues)](https://github.com/KennyOliver/VividHues)

---
Kenny Oliver ©2021
